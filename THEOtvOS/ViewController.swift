//
//  ViewController.swift
//  THEOtvOS
//
//  Created by Laurent Wauters on 19/03/2019.
//  Copyright © 2019 THEO Technologies NV. All rights reserved.
//

import UIKit
import THEOplayerSDK

class ViewController: UIViewController {

    var theoplayer: THEOplayer!
    let timeSkip = 10.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initialize Player
        theoplayer = THEOplayer(with: CGRect(x: (UIScreen.main.bounds.width - 1500)/2, y: 20, width: 1500, height: 1500/16*9), configuration: THEOplayerConfiguration(chromeless: false))
        //Add player as subView
        theoplayer.addAsSubview(of: view)
        
        //Basic Source Configuration
        let typedSource = TypedSource(src: "https://cdn.theoplayer.com/video/elephants-dream/playlist.m3u8", type: "application/x-mpegurl")
        theoplayer.source = SourceDescription(source: typedSource)
        
        //Play the player
        theoplayer.play()
        
        
        // Create some controls
        let playButton = UIButton(type: .system)
        playButton.frame = CGRect(x: 70, y: 985, width: 300, height: 70)
        playButton.setTitle("Play", for: UIControl.State.normal)
        playButton.addTarget(self, action: #selector(self.play), for: .primaryActionTriggered)
        
        let pauseButton = UIButton(type: .system)
        pauseButton.frame = CGRect(x: 440, y: 985, width: 300, height: 70)
        pauseButton.setTitle("Pause", for: UIControl.State.normal)
        pauseButton.addTarget(self, action: #selector(self.pause), for: .primaryActionTriggered)
        
        let fsButton = UIButton(type: .system)
        fsButton.frame = CGRect(x: 810, y: 985, width: 300, height: 70)
        fsButton.setTitle("Fullscreen", for: UIControl.State.normal)
        fsButton.addTarget(self, action: #selector(self.fullscreen), for: .primaryActionTriggered)
        
        let skipButton = UIButton(type: .system)
        skipButton.frame = CGRect(x: 1180, y: 985, width: 300, height: 70)
        skipButton.setTitle("<<Rewind", for: UIControl.State.normal)
        skipButton.addTarget(self, action: #selector(self.reverse), for: .primaryActionTriggered)
        
        let skipForwardButton = UIButton(type: .system)
        skipForwardButton.frame = CGRect(x: 1550, y: 985, width: 300, height: 70)
        skipForwardButton.setTitle("Forward>>", for: UIControl.State.normal)
        skipForwardButton.addTarget(self, action: #selector(self.skipForward), for: .primaryActionTriggered)
        
        self.view.addSubview(playButton)
        self.view.addSubview(pauseButton)
        self.view.addSubview(skipButton)
        self.view.addSubview(skipForwardButton)
        self.view.addSubview(fsButton)
        
        
    }
    
    //Button Functions
    @objc private func play() {
        theoplayer.play()
    }
    
    @objc private func pause() {
        theoplayer.pause()
    }
    
    @objc private func reverse() {
        theoplayer.requestCurrentTime { (currentTime, error) in
            self.theoplayer.setCurrentTime(currentTime! - self.timeSkip);
        }
        
    }
    
    @objc private func skipForward() {
        theoplayer.requestCurrentTime { (currentTime, error) in
            self.theoplayer.setCurrentTime(currentTime! + self.timeSkip);
        }
    }
    
    @objc private func fullscreen() {
        theoplayer.presentationMode = .fullscreen
        
        if let keyWindow = UIApplication.shared.keyWindow {
            let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swiped))
            swipeRecognizer.direction = .up
            keyWindow.rootViewController?.view.addGestureRecognizer(swipeRecognizer)
        }
    }
    
    @objc func swiped() {
        theoplayer.presentationMode = .inline
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        theoplayer.destroy()
    }
}
